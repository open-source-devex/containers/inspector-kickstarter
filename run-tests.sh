#!/usr/bin/env sh

set -v
set -e

CONTAINER_NAME="$1"
CONTAINER_TEST_IMAGE="$2"

docker rm -f ${CONTAINER_NAME} 2>&1 >/dev/null || true

docker run --rm --name ${CONTAINER_NAME} \
    -d ${CONTAINER_TEST_IMAGE}

# tests
docker exec -t ${CONTAINER_NAME} ash -c 'python3.7 -V'

# clean up
docker rm -f ${CONTAINER_NAME}
