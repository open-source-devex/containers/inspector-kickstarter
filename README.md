# Inspector Kickstarter

Python container for kicking off Inspector scans based on what's running in your environment.

- fetches AMIs in the region it's deployed in,
- starts new VMs with this image and the inspector agent
- kicks off an inspector scan
- published some finding metadata about high findings to an SNS topic
